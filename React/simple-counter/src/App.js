import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

//добавить шаг изменеия процентов от 1-10 (второй счетчик)!!!


class App extends Component {
    state = {
        number: 50

    };


  render() {
      const {number} = this.state;
    return (
        <div className="App">
          <button onClick={this.decrease}> - </button>
          <span>{number}%</span>
          <button onClick={this.increase}> + </button>
        </div>
    );



  }

    increase = () => {
        const {number} = this.state;
      this.setState({number: number + 1});
    };

    decrease = () => {
        const {number} = this.state;
      this.setState({number: number - 1});
    }
}


export default App;
