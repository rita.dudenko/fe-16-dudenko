import React, {PureComponent} from 'react';

class Loading extends PureComponent {
    render() {
        return (
            <h2>
                Loading...
            </h2>
        );
    }
}

export default Loading;