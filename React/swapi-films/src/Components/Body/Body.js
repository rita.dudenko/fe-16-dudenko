import React, {PureComponent} from 'react';



class Body extends PureComponent {
    state = {
        expanded: false,
    };

    render() {
        const {films, isLoading} = this.props;
        const {expanded} = this.state;
        const filmInfo = films.map(film => <li key={film.id}>{film.name}<button onClick={this.showDetails}>Детальнее</button></li>);
        return (
            <ul>{filmInfo}</ul>
        );
    }

    showDetails = () => {
        this.setState({expanded: true});

    }
}

export default Body;