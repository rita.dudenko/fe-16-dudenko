import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Loading from "./Components/Loading/Loading";
import Body from "./Components/Body/Body";

const API = 'https://ajax.test-danit.com/api/swapi/films';

class App extends Component{
    state = {
        films: [],
        isLoading: true,
    };

  render() {
      const {films, isLoading} = this.state;
    return (
        <div className="App">
            {isLoading && <Loading/>}
            {!isLoading &&  <Body films={films} isLoading={isLoading}/>}
        </div>
    );
  }

  componentDidMount() {
      const {films} = this.state;
      fetch(`${API}`)
          .then(data => data.json())
          .then(filmData => this.setState({films: filmData, isLoading: false}));
  }
}


export default App;
