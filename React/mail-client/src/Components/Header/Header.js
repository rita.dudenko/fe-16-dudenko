import React, {Fragment,PureComponent} from 'react';
import PropTypes from 'prop-types';


//props бьект, в котором хранятся данные, переданные с родительского компонента (ИЗНАЧАЛЬНО ПУСТОЙ)
class Header extends PureComponent {
    render() {
        const {name, age, email} = this.props;
        //или пустой тег
        return (
            <Fragment>
                <h2>
                    Header
                </h2>
                <div>{name}</div>
                <div>{age}</div>
                <div>{email}</div>
            </Fragment>
        );
    }
}

Header.propTypes ={
    name: PropTypes.string.isRequired,
    age: PropTypes.number
}



export default Header;