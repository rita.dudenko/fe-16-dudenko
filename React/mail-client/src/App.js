import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Header from "./Components/Header/Header";
import Main from "./Components/Main/Main";
import Footer from "./Components/Footer/Footer";

const emails = [
    {id:1, topic: "Email 1"},
    {id:2, topic: "Email 2"},
    {id:3, topic: "Email 3"},
    {id:4, topic: "Email 4"}

];

class App extends Component{
    state = {
        currentUser: {
            name: "Rita",
            age: 18,
            email: 'gugbfgf@gmail.com'
        },
        title: "Hello"
    };


render() {

        //boolean, null, undefined в JSX не рендерется!!!
        const {currentUser} = this.state;
        const {name, age, email} = currentUser;
        const hasEmail = !!email;

        return (
            <div className="App">
                <Header user = {currentUser}/>
                <Main icrementAge = {this.incrementAge} updateTitle = {this.updateTitle} emails={emails}/>
                <Footer/>
            </div>
        );
    }
}



export default App;
