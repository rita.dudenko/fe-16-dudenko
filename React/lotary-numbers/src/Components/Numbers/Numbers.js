import React, {PureComponent} from 'react';
import Button from "../Button/Button";

class Numbers extends PureComponent {
    render() {
        const{numbers} = this.props;
       const numberItems = numbers.map ((number, index) => {
           <li key={index}>
               {number}<Button title = 'x'/>
           </li>
       });
        return (
            <ul>
                    {numberItems}
                </ul>

        );
    }


}

export default Numbers;