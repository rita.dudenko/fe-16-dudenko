import React, {Component} from 'react';

import './App.css';
import Numbers from "./Components/Numbers/Numbers";
import Button from "./Components/Button/Button";




 class App extends Component{
     state = {
         numberArr: []
     };

  render() {
      const {numberArr} = this.state;
    return (
        <div className="App">
          <Button  title = 'Generate' onclick = {this.generateNumber}/>
          <Numbers numbers={numberArr} />
        </div>
    );
  }

     generateNumber = () => {
      const {numberArr} = this.state;
      const number = Math.ceil(Math.random()*36);
        this.setState({numberArr: [...numberArr, number]});
  };

  deleteNumber = (index) => {

  }



}

export default App;
