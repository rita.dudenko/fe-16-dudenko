const {sortCollection, collection, param} = require('../../ex-1');


describe('sortCollection', () => {
    it('should be a function',() => {
        expect(typeof sortCollection).toEqual('function');


    });



    // let sortedCollection;
    // it('should return collection with the same lenghts', () => {
    //     sortedCollection = sortCollection(collection, 'name', 'asc');
    //     expect(sortedCollection.length).toEqual(collection.length);
    // });

    describe('parametrs', () => {
        let collection ;
        beforeEach(() =>
            collection = [{name: 'Ivan'},{name: 'Anna'},{name: 'Olya'},{name: 'Pasha'}]
        );
        it('collection must be defined',() => {
            expect(collection).toBeDefined()
        });

        it('collection must be an array', () => {
            expect(Array.isArray(collection)).toBeTruthy()
        });


        let prop = 'name';
        it('collection items must be an objects', () =>{
            expect(collection.every(item => typeof item === 'object' && item !== null)).toBeTruthy();
            expect(collection.every(item => item.hasOwnProperty([prop]))).toBeTruthy()

        });




    })



});