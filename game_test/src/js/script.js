let app, gameScene, gameWinScene, fruit1, fruit12, fruit13, fruit2, fruit22, fruit23,
    fruit3, fruit32, fruit33, fruit4, fruit42, fruit43, fruit5, fruit52, fruit53,
    wildSymbol,  wildSymbol2,  wildSymbol3, game, spinBtn, fruitList, fruitList2, fruitList3;

gameCteation();

function gameCteation() {
    app = new PIXI.Application({
            width: 764,
            height: 428,
            antialias: true,
            transparent: false,
            resolution: 1

        }
    );
    document.body.append(app.view);
    loadGame();
}

function loadGame(){
    PIXI.loader
        .add("assets/gameImages.json")
        .load(setup);

        let message = new PIXI.Text("Loading...");
        message.style = {fill: "white", fontSize: "46px"};
        message.position.set(284, 190);
        app.stage.addChild(message);

}




function setup() {
  let id = PIXI.loader.resources["assets/gameImages.json"].textures;

    gameScene = new PIXI.Container();
    app.stage.addChild(gameScene);

    gameWinScene = new PIXI.Container();
    app.stage.addChild(gameWinScene);
    gameWinScene.visible = false;

    game = new PIXI.Sprite(id["BG.png"]);
    game.scale.set(0.8, 0.8);

    let fruitCol1 = new PIXI.Container();
    fruitCol1.position.set(97,-130);

    let fruitCol2 = new PIXI.Container();
    fruitCol2.position.set(340,-130);

    let fruitCol3 = new PIXI.Container();
    fruitCol3.position.set(570,-130);


    fruit1 = new PIXI.Sprite(id["SYM7.png"]);
    fruit12 = new PIXI.Sprite(id["SYM7.png"]);
    fruit13 = new PIXI.Sprite(id["SYM7.png"]);
    fruit2 = new PIXI.Sprite(id["SYM3.png"]);
    fruit22 = new PIXI.Sprite(id["SYM3.png"]);
    fruit23 = new PIXI.Sprite(id["SYM3.png"]);
    fruit3 = new PIXI.Sprite(id["SYM4.png"]);
    fruit32 = new PIXI.Sprite(id["SYM4.png"]);
    fruit33 = new PIXI.Sprite(id["SYM4.png"]);
    fruit4 = new PIXI.Sprite(id["SYM5.png"]);
    fruit42 = new PIXI.Sprite(id["SYM5.png"]);
    fruit43 = new PIXI.Sprite(id["SYM5.png"]);
    fruit5 = new PIXI.Sprite(id["SYM6.png"]);
    fruit52 = new PIXI.Sprite(id["SYM6.png"]);
    fruit53 = new PIXI.Sprite(id["SYM6.png"]);
    wildSymbol = new PIXI.Sprite(id["SYM1.png"]);
    wildSymbol2 = new PIXI.Sprite(id["SYM1.png"]);
    wildSymbol3 = new PIXI.Sprite(id["SYM1.png"]);

    fruitList = [fruit1, fruit2, fruit3, fruit4, fruit5, wildSymbol];
    fruitList2 = [fruit12, fruit22, fruit32, fruit42, fruit52, wildSymbol2];
   fruitList3 = [fruit13, fruit23, fruit33, fruit43, fruit53, wildSymbol3];

   addFuitsIntoCol(fruitList, fruitCol1);
    addFuitsIntoCol(fruitList2, fruitCol2);
    addFuitsIntoCol(fruitList3, fruitCol3);


    spinBtn = new PIXI.Sprite(id["BTN_Spin.png"]);
    spinBtn.position.set(824, 220);
    spinBtn.interactive = true;
    spinBtn.buttonMode = true;


    gameScene.addChild(game);
    game.addChild(fruitCol1);
    game.addChild(fruitCol2);
    game.addChild(fruitCol3);
    game.addChild(spinBtn);

    play();

}



function play() {
    gameWinScene.visible = false;
    clearGameCol(fruitList);
    clearGameCol(fruitList2);
    clearGameCol(fruitList3);

    let firstInterval = showRandomFruit(fruitList);
    let secondInterval = showRandomFruit(fruitList2);
    let thirdInterval = showRandomFruit(fruitList3);

    setTimeout(() => {
       let index1 = showChoosenFruit(fruitList, firstInterval);
       let index2 = showChoosenFruit(fruitList2, secondInterval);
       let index3 = showChoosenFruit(fruitList3, thirdInterval);

        if ((index1 === index2 && index1 === index3) || (index1 === index2 && index3 === 5) || (index1 === index3 && index2 === 5)
            || (index2 === index3 && index1 === 5)){
            gameWin();
        }

        }, 3000);


    spinBtn.on("pointerdown", play);

}

function showRandomFruit(fruitList){
    return setInterval(() => {
        let index = randomInt(0, fruitList.length-1);
        app.ticker.add(() => gameLoop(fruitList[index]));
    }, 500);

}

function showChoosenFruit(fruitList, interval) {
    let index = randomInt(0, fruitList.length-1);
    clearInterval(interval);
    app.ticker.add(() => getFruit(fruitList[index]));
    return index;
}

function gameLoop(fruit) {
    fruit.y += 8;
    if(fruit.y >= 400){
        fruit.y = -30;
    }

    setTimeout(() => {
        fruit.y = -30;
    }, 1500);

}

function getFruit(fruit) {
    fruit.y = 350;
}


function addFuitsIntoCol(fruitList, col){
    fruitList.forEach((fruit) => {
        fruit.scale.set(0.8, 0.8);
        col.addChild(fruit);
    });
}

function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1));
}



function gameWin() {
    gameWinScene.visible = true;
    let winContainer = new PIXI.Container();
    let message = new PIXI.Text("You Win");
    message.style = {fill: "white", fontSize: "46px"};
    message.position.set(284, 190);
    gameWinScene.addChild(winContainer);
    winContainer.addChild(message);

}

function clearGameCol(fruitList){
     fruitList.forEach(fruit => fruit.y = -130);
}





