const {src, dest, series, parallel, watch} = require('gulp');
const del = require('del');
const browserSync = require('browser-sync').create();


function clearDist(){
    return del('./dest');
}

function copyIndex(){
    return src('./src/index.html')
        .pipe(dest('./dest'))
        .pipe(browserSync.reload({ stream: true }));
}


function clearJs() {
    return del('./dest/js/**');

}


function copyJs(){
    return src('./src/js/*.js')
        .pipe(dest('./dest/js'))
        .pipe(browserSync.reload({ stream: true }));
}





function clearAssets() {
    return del('./dest/assets/**');

}

function copyAssets() {
    return src('./src/assets/**')
        .pipe(dest('./dest/assets'))
        .pipe(browserSync.reload({ stream: true }));


}

const changeAssets = series(clearAssets,copyAssets,copyIndex);
const changeScript = series(clearJs,copyJs,copyIndex);


function serve(){
    browserSync.init({
        server: "./dest"
    });

    watch('./src/assets/**/*.png', changeAssets);
    watch('./src/js/*.js',changeScript);
    watch('./src/index.html',copyIndex);


}


const build = series(clearDist,parallel(copyAssets,copyJs),copyIndex);
exports.build = build;
exports.dev = series(build, serve);